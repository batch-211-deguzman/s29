/*
	1

*/


db.users.find({$or: [{firstName: {$regex: 's', $options: 'i'}}, {lastName: {$regex: 'd', $options: 'i'}}]});

/*
	2
*/


db.users.find({$and: [{department: {$regex: 'HR', $options: 'i'}}, {age: {$gte: 70}}]});

/*
	3
*/


db.users.find({$and: [{firstName: {$regex: 'e', $options: 'i'}}, {age: {$lte: 30}}]});

