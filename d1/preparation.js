// For easier code along and activity tomorrow, please do the following instructions :D 

// 1. Open your Robo3T. Once the application is open, right click on the course_booking database and open the mongo shell in order to execute commands. 

// 1. Delete all of the contents of the database under course_booking. Type this command on your mongo shell. 
db.users.deleteMany({});


// 2. Insert the following (as a new content) in your database
db.users.insertMany([
{
    firstName : "Jane",
    lastName : "Doe",
    age : 21.0,
    contact : {
        "phone" : "87654321",
        "email" : "janedoe@gmail.com"
    },
    courses : [ 
        "CSS", 
        "Javascript", 
        "Python"
    ],
    department : "HR"
},
{
    firstName : "Stephen",
    lastName : "Hawking",
    age : 76.0,
    contact : {
        "phone" : "87654321",
        "email" : "stephenhawking@gmail.com"
    },
    courses : [ 
        "Python", 
        "React", 
        "PHP"
    ],
    department : "HR"
},
{
    firstName : "Neil",
    lastName : "Armstrong",
    age : 82.0,
    contact : {
        phone : "87654321",
        email : "neilarmstrong@gmail.com"
    },
    courses : [ 
        "React", 
        "Laravel", 
        "Sass"
    ],
    department : "HR"
},
{
    firstName : "Bill",
    lastName : "Gates",
    age : 65.0,
    contact : {
        phone : "12345678",
        email : "bill@gmail.com"
    },
    courses : [ 
        "PHP", 
        "Laravel", 
        "HTML"
    ],
    department : "Operations"
}
]);
